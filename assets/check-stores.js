(function() {

  var stockTableArray = [];
  var colorNameIdxMap = [];

  function getBranchName(rawBranchName) {
    var branchMatches = rawBranchName.match(/Juliette Hogan (.*?)(?:,|$)/i);

    return branchMatches.length ? branchMatches[1] : el.Branch;
  }

  // Return the slide index based on the selected color if we are on product page, otherwise just return 0
  function getFancyboxSlideIdx($target) {
    var selectedColorName = jQuery('input[name="option-0"]:checked').val();
    var productId         = $target.data('product-id');

    return $target.is('#product-page-check-stores-availability') ?
      colorNameIdxMap[productId][selectedColorName] : 0;
  }

  $('body').on('click', '#product-page-check-stores-availability, .js-check-stores-availability', function(e) {
    e.preventDefault();

    var $this       = $(this);
    var productId   = $this.data('product-id');
    var variantSkus = $this.data('variant-skus');

    if (!stockTableArray[productId]) {
      stockTableArray[productId] = [];
      colorNameIdxMap[productId] = {};
    }

    if (stockTableArray[productId].length) {
      $.fancybox(stockTableArray[productId], {
        index : getFancyboxSlideIdx($this),
        arrows: false,
      });
      return;
    }

    // Compose the API URL for Cin7
    var cin7Where = "code='" + variantSkus.join("' or code='") + "'";
    var cin7Url   = 'https://api.cin7.com/cloud/APILite/APILite.ashx'
      + '?apiid=215997'
      + '&apikey=cPun2vgm7jOlDC2nR16Lmue915JlcIuM'
      + '&action=GetStock'
      + '&orderby=Option1,Branch,Sizes'
      + '&where=' + encodeURIComponent(cin7Where);

    // Parse the result CSV into object using Papa Parse
    Papa.parse(cin7Url, {
      download     : true,
      header       : true,
      dynamicTyping: true,
      complete     : function(results) {
        var stock = {};

        // Initialise stock, just in case the API doesn't return all possible colors branches or sizes.
        // If products are not linked to a given branch in Cin7, the stock info won't be returned via API,
        // so we need to fill those missing records with "out of stock" as default.
        var options         = $this.data('options');
        var colorOptions    = options[0].values;
        var sizeOptions     = options[1].values;
        var defaultBranches = {
          'britomart': {
            tel  : '09 377 3326',
            sizes: {},
          },
          'newmarket': {
            tel  : '09 522 8203',
            sizes: {},
          },
          'ponsonby' : {
            tel  : '09 360 9347',
            sizes: {},
          },
          'wellington' : {
            tel  : '04 473 4399',
            sizes: {},
          }
        };

        colorOptions.forEach(function(c) {
          c = c.toLowerCase();

          stock[c] = $.extend(true, {}, defaultBranches); // Clone the object

          for (var b in stock[c]) {
            sizeOptions.forEach(function(s) {
              s = s.toLowerCase();

              stock[c][b]['sizes'][s] = 0;
            });
          }
        });

        results.data.forEach(function(el) {
          var color      = el.Option1.toLowerCase();
          var branchName = getBranchName(el.Branch).toLowerCase();
          var size       = (el.Sizes + '').toLowerCase();

          // Skip the Workroom and Online branches
          if (branchName === 'workroom' || branchName === 'online') {
            return true;
          }

          // Catch any new colors we didn't have in the initialised the stock object
          if (typeof stock[color] === 'undefined') {
            stock[color] = $.extend(true, {}, defaultBranches);
          }

          // Catch any new branches
          for (var c in stock) {
            if (typeof stock[c][branchName] === 'undefined') {
              stock[c][branchName] = {tel: '', sizes: {}};
            }
          }

          // Catch any new sizes
          for (c in stock) {
            for (var bn in stock[c]) {
              if (typeof stock[c][bn]['sizes'][size] === 'undefined') {
                stock[c][bn]['sizes'][size] = 0;
              }
            }
          }

          stock[color][branchName]['sizes'][size] = el.SOH - el.OnOrder;
        });

        // Render the in store availability tables using Handlebars and Fancybox
        Handlebars.registerHelper('renderQty', function(qty) {
          if (qty > 1) {
            return '<span class="check-stores__in-stock">✓</span>';
          } else if (qty === 1) {
            return '<span class="check-stores__nearly-gone">●</span>';
          } else {
            return '<span class="check-stores__out-of-stock">✗</span>';
          }
        });

        Handlebars.registerHelper('if_eq', function(a, b, opts) {
          if (a === b)
            return opts.fn(this);
          else
            return opts.inverse(this);
        });

        var template = Handlebars.compile($('#check-stores-tmpl').html());
        var idx      = 0;

        // Generate the color swatch background URL list
        var colors = {};
        for (var c in stock) {
          colors[c] = '//cdn.shopify.com/s/files/1/0605/9245/t/15/assets/' + c.replace(' ', '-') + '.png';
        }

        for (var color in stock) {
          var stockTable = template({
            currentColor: color,
            branches    : stock[color],
            colors      : colors,
            productId   : productId,
          });

          stockTableArray[productId].push(stockTable);
          colorNameIdxMap[productId][color] = idx++;
        }

        $.fancybox(stockTableArray[productId], {
          index : getFancyboxSlideIdx($this),
          arrows: false,
        });
      }
    });
  }).on('click', '.js-check-stores__swatch-wrapper', function() {
    var $this     = $(this);
    var productId = $this.data('product-id');
    var color     = $this.data('color');

    $.fancybox.jumpto(colorNameIdxMap[productId][color]);
  });
})();
